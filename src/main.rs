#[macro_use]
extern crate conrod_core;
extern crate conrod_glium;
extern crate conrod_winit;
extern crate conrod_derive;
extern crate find_folder;
extern crate glium;

mod support;
mod widgets;

use glium::Surface;

const WIN_W: u32 = 800;
const WIN_H: u32 = 600;

widget_ids! {
    struct Ids {
        master,
        rect00,
        rect01,
        cube00,
        text,
        map,
    }
}

fn set_ui(ref mut ui: conrod_core::UiCell, ids: &Ids, elapsed: f64) {
    use conrod_core::{color, widget, Colorable, Positionable, Sizeable, Widget};
    widget::Canvas::new()
        .color(color::rgb(0.2, 0.2, 0.2))
        .set(ids.master, ui);
    //widget::Rectangle::fill([10.0, 10.0])
    //    .xy([100.0, 100.0])
    //    .w_h(200.0, 100.0)
    //    .color(color::RED)
    //    .set(ids.rect01, ui);
    //widgets::isocube::iso_cube::IsoCube::new()
    //    .xy([100.0, 100.0])
    //    .w_h(200.0, 100.0)
    //    .set(ids.cube00, ui);
    //widget::Rectangle::fill([10.0, 10.0])
    //    .xy([-100.0, 100.0])
    //    .w_h(200.0, 200.0)
    //    .color(color::BLUE)
    //    .set(ids.rect00, ui);
    let d = 32;
    widgets::isomap::iso_map::IsoMap::new(d, d)
        .x_y(0.0, 0.0)
        .w_h(800.0, 600.0)
        .set(ids.map, ui);
    let fps_as_string = format!("{:.0}", 1.0 / elapsed);
    widget::Text::new(&fps_as_string)
        .parent(ids.master)
        .top_left_with_margins(3.0, 5.0)
        .set(ids.text, ui);
    /*
    use conrod_core::widget::primitive::shape::triangles;
    let s = 100.0;
    let vs: Vec<(f64, f64)> = vec![
        (-1.0,1.0),
        (0.0,2.0),
        (1.0,1.0),
        (1.0,0.0),
        (0.0,-1.0),
        (-1.0,0.0),
    ];
    let v0 = (0.0, 0.0);
    let mut v: Vec< triangles::Triangle::<_> > = vec![];
    for i in 0..vs.len() {
        let v1 = vs[i];
        let v2 = if i + 1 < vs.len() {
            vs[i + 1]
        } else {
            vs[0]
        };
        let t = triangles::Triangle::from(
            [([v0.0 * s, v0.1 * s], color::RED.to_rgb()),
            ([v1.0 * s, v1.1 * s], color::GREEN.to_rgb()),
            ([v2.0 * s, v2.1 * s], color::GREEN.to_rgb())],
            );
        v.push(t);
    }
    let ts = widget::Triangles::multi_color(v.into_iter());
    ts.calc_bounding_rect().set(ids.cube00, ui);
    */

    //widget::Text::new("blah")
    //    .color(color::WHITE)
    //    .padded_kid_area_wh_of(ids.master, 10.0)
    //    .set(ids.text, ui);
}

fn main() {
    println!("Hello, world!");

    // Build the window.
    let mut events_loop = glium::glutin::EventsLoop::new();
    let window = glium::glutin::WindowBuilder::new()
        .with_title("Conrod with glium!")
        .with_dimensions((WIN_W, WIN_H).into());
    let context = glium::glutin::ContextBuilder::new()
        .with_vsync(true)
        .with_multisampling(4);
    let display = glium::Display::new(window, context, &events_loop).unwrap();
    let display = support::GliumDisplayWinitWrapper(display);

    // Construct our `Ui`.
    let mut ui = conrod_core::UiBuilder::new([WIN_W as f64, WIN_H as f64])
        //.theme(conrod_example_shared::theme())
        .build();

    // A unique identifier for each widget.
    let ids = Ids::new(ui.widget_id_generator());

    // Add a `Font` to the `Ui`'s `font::Map` from file.
    let assets = find_folder::Search::KidsThenParents(3, 5)
        .for_folder("assets")
        .unwrap();
    let font_path = assets.join("fonts/NotoSans/NotoSans-Regular.ttf");
    ui.fonts.insert_from_file(font_path).unwrap();

    // A type used for converting `conrod_core::render::Primitives` into `Command`s that can be used
    // for drawing to the glium `Surface`.
    let mut renderer = conrod_glium::Renderer::new(&display.0).unwrap();

    // The image map describing each of our widget->image mappings (in our case, none).
    let image_map = conrod_core::image::Map::<glium::texture::Texture2d>::new();

    let mut event_loop = support::EventLoop::new();
    let mut timer_start = std::time::Instant::now();
    'main: loop {
        // Handle all events.
        for event in event_loop.next(&mut events_loop) {
            // Use the `winit` backend feature to convert the winit event to a conrod one.
            if let Some(event) = support::convert_event(event.clone(), &display) {
                ui.handle_event(event);
                event_loop.needs_update();
            }

            match event {
                glium::glutin::Event::WindowEvent { event, .. } => match event {
                    // Break from the loop upon `Escape`.
                    glium::glutin::WindowEvent::CloseRequested
                    | glium::glutin::WindowEvent::KeyboardInput {
                        input:
                            glium::glutin::KeyboardInput {
                                virtual_keycode: Some(glium::glutin::VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    } => break 'main,
                    _ => (),
                },
                _ => (),
            }
        }

        event_loop.needs_update();

        let elapsed = timer_start.elapsed().as_secs_f64();
        timer_start = std::time::Instant::now();

        // Instantiate a GUI demonstrating every widget type provided by conrod.
        //conrod_example_shared::gui(&mut ui.set_widgets(), &ids, &mut app);
        set_ui(ui.set_widgets(), &ids, elapsed);

        // Draw the `Ui`.
        let primitives = ui.draw(); {
            renderer.fill(&display.0, primitives, &image_map);
            let mut target = display.0.draw();
            target.clear_color(0.3, 0.0, 0.3, 1.0);
            renderer.draw(&display.0, &mut target, &image_map).unwrap();
            target.finish().unwrap();
        }
    }
}
