extern crate conrod_core;
extern crate conrod_derive;

pub mod iso_cube
{
    use conrod_core::{color, widget, Color, Range, Rect, Widget};

    #[derive(Copy, Clone, Debug, Default, PartialEq, WidgetStyle)]
    pub struct Style
    {
        pub color: Option<Color>,
    }

    #[derive(WidgetCommon)]
    pub struct IsoCube
    {
        #[conrod(common_builder)]
        common: widget::CommonBuilder,
        style: Style,
        length: f64,
        uv_rect: [f64; 4],
    }

    widget_ids! {
        struct Ids {
            cube,
        }
    }

    use conrod_core::widget::primitive::shape::{triangles};

    pub struct State {
        ids: Ids,
        prev_rect: Rect,
        prev_length: f64,
        triangles: Vec::<triangles::Triangle<triangles::ColoredPoint>>,
    }

    impl IsoCube {
        pub fn new() -> Self {
            IsoCube {
                common: widget::CommonBuilder::default(),
                style: Style::default(),
                length: 0.0,
                uv_rect: [0.0; 4],
            }
        }

        pub fn uv_rect(mut self, x: f64, y: f64, w: f64, h: f64) -> Self {
            self.uv_rect = [x, y, w, h];
            self
        }

        pub fn length(mut self, l: f64) -> Self {
            self.length = l;
            self
        }

        fn compute_triangles(&self, rect: &Rect, length: f64)
            -> Vec::<triangles::Triangle<triangles::ColoredPoint>>
        {
            fn l2w(l: f64) -> f64 {
                let w = 1.0 - 1.0 / ((0.25 * l) + 1.0);
                w * w
            }

            fn make_vertex(x: f64, y: f64, uvw: (f64, f64, f64)) -> ([f64;2], color::Rgba) {
                fn lerp(a: color::Rgba, b: color::Rgba, f: f64) -> color::Rgba {
                    fn lerp_c(a: f32, b: f32, f: f64) -> f32 {
                        a + (b - a) * f32::min(1.0, f32::max(0.0, f as f32))
                    }
                    color::Rgba(
                        lerp_c(a.0, b.0, f),
                        lerp_c(a.1, b.1, f),
                        lerp_c(a.2, b.2, f),
                        1.0
                    )
                }
                let c_x0 = color::rgb_bytes(0x14, 0x46, 0xa0).to_rgb();
                let c_x1 = color::rgb_bytes(0x37, 0x80, 0xc6).to_rgb();
                let c_x = lerp(c_x0, c_x1, uvw.0);
                let c_y0 = color::rgb_bytes(0xdb, 0x30, 0x69).to_rgb();
                let c_y1 = color::rgb_bytes(0x0c, 0x22, 0x4b).to_rgb();
                let c_y = lerp(c_y0, c_y1, uvw.1);
                let c_xy = lerp(c_x, c_y, 0.5 + 0.5 * (uvw.0 - uvw.1));
                let c_z1 = color::rgb_bytes(0xf5, 0xd5, 0x47).to_rgb();
                //let c_z2 = color::rgb_bytes(0xfa, 0xf1, 0x99).to_rgb();
                //let mid_height = l2w(4.0);
                //let c = if uvw.2 > mid_height {
                //    lerp(c_z1, c_z2, (uvw.2 - mid_height) / (1.0 - mid_height))
                //} else {
                //    lerp(c_xy, c_z1, uvw.2 / mid_height)
                //};
                let c = lerp(c_xy, c_z1, uvw.2);
                ([x, y], c.into())
            }

            // vertex coordinates
            let [x_off, y_off] = rect.bottom_left();
            let (x_scale, y_scale) = rect.w_h();
            let sqrt3_2 = 0.8660254037844; // sqrt(3) / 2
            let x0 = (1.0 - sqrt3_2) / 2.0;
            let x1 = x0 + sqrt3_2 / 2.0;
            let x2 = x0 + sqrt3_2;
            let offset = length * 0.25;

            let vs: [(f64, f64);6] = [
                (x0, 0.75 + offset),
                (x1, 1.0 + offset),
                (x2, 0.75 + offset),
                (x2, 0.25),
                (x1, 0.0),
                (x0, 0.25),
            ];

            // vertex uv
            let (top, left, bottom, right) = (
                self.uv_rect[0],
                self.uv_rect[1],
                self.uv_rect[2] + self.uv_rect[0],
                self.uv_rect[3] + self.uv_rect[1]
            );

            let high = l2w(1.0 + length);
            let uvws: [(f64, f64, f64); 6] = [
                (right, top, high),
                (left, top, high),
                (left, bottom, high),
                (left, bottom, 0.0),
                (right, bottom, 0.0),
                (right, top, 0.0),
            ];

            let v0 = (x1, 0.5 + offset);
            let uvw0 = (right, bottom, high);

            (0..vs.len()).map(|i| {
                let (v1, uvw1) = (vs[i], uvws[i]);
                let (v2, uvw2) = if i + 1 < vs.len() {
                    (vs[i + 1], uvws[i + 1])
                } else {
                    (vs[0], uvws[0])
                };
                //let (c1, c2) = if (i % 2) == 0 {
                //    (color::RED.to_rgb(), color::GREEN.to_rgb())
                //} else {
                //    (color::GREEN.to_rgb(), color::RED.to_rgb())
                //};
                triangles::Triangle::from([
                    make_vertex(x_off + v0.0 * x_scale, y_off + v0.1 * y_scale, uvw0),
                    make_vertex(x_off + v1.0 * x_scale, y_off + v1.1 * y_scale, uvw1),
                    make_vertex(x_off + v2.0 * x_scale, y_off + v2.1 * y_scale, uvw2)
                ])
            }).collect()
        }
    }

    impl Widget for IsoCube {
        type State = State;
        type Style = Style;
        type Event = ();

        fn init_state(&self, id_gen: conrod_core::widget::id::Generator) -> Self::State {
            let empty_range = Range { start: 0.0, end: 0.0 };
            State {
                ids: Ids::new(id_gen),
                prev_length: 0.0,
                prev_rect: Rect { x: empty_range, y: empty_range },
                triangles: vec![],
            }
        }

        fn style(&self) -> Self::Style {
            self.style.clone()
        }

        fn update(self, args: widget::UpdateArgs<Self>) -> Self::Event {
            let widget::UpdateArgs { id: _, state, rect, ui, style: _, .. } = args;
            if rect != state.prev_rect || self.length != state.prev_length {
                state.update(|state| {
                    let mut length_delta = state.prev_length - self.length;
                    if f64::abs(length_delta) < 0.01 {
                        length_delta = 0.0;
                    }
                    state.prev_rect = rect;
                    state.prev_length -= length_delta / 5.0;
                    state.triangles = self.compute_triangles(&rect, state.prev_length);
                });
            }
            let ts = state.triangles.clone();
            let ts = widget::Triangles::multi_color(ts.into_iter());
            ts.with_bounding_rect(rect).set(state.ids.cube, ui);
        }
    }
}
