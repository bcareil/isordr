extern crate conrod_core;
extern crate conrod_derive;

pub mod iso_map
{
    use crate::widgets::isocube::iso_cube;
    #[allow(unused_imports)]
    use conrod_core::{color, widget, Widget};
    use rand::prelude::*;

    #[derive(Copy, Clone, Debug, Default, PartialEq, WidgetStyle)]
    pub struct Style {
    }

    #[derive(WidgetCommon)]
    pub struct IsoMap {
        #[conrod(common_builder)]
        common: widget::CommonBuilder,
        style: Style,
        width: u32,
        height: u32,
    }

    widget_ids! {
        struct Ids {
            cubes[],
            dbg_sq0,
            dbg_sq1,
            dbg_sq2,
        }
    }

    #[allow(dead_code)]
    pub struct State {
        height_map: Vec<i8>,
        last_update: std::time::Instant,
        cells: Vec<iso_cube::IsoCube>,
        ids: Ids,
    }

    impl IsoMap {
        pub fn new(w: u32, h: u32) -> Self {
            IsoMap {
                common: widget::CommonBuilder::default(),
                style: Style::default(),
                width: w,
                height: h,
            }
        }

        pub fn allocate_cells(&self) -> Vec<iso_cube::IsoCube> {
            let mut v = vec![];
            v.reserve((self.width * self.height) as usize);
            for _y in 0..self.height {
                for _x in 0..self.width {
                    v.push(iso_cube::IsoCube::new());
                }
            }
            return v;
        }

        #[allow(dead_code)]
        pub fn generate_height_map(&self) -> Vec<i8> {
            let mut v = vec![0i8; (self.width * self.height) as usize];
            let mut rng = rand::thread_rng();
            let min_height: i32 = 2;
            let max_height: i32 = 7;
            let height_range = (max_height - min_height) as u32;
            v.reserve((self.width * self.height) as usize);
            for c in v.iter_mut() {
                *c = ((rng.next_u32() % height_range) as i32 + min_height) as i8;
            }
            return v;
        }

        #[allow(dead_code)]
        pub fn flat_height_map(&self) -> Vec<i8> {
            vec![3i8; (self.width * self.height) as usize]
        }

        #[allow(dead_code)]
        pub fn shape_height_map(&self) -> Vec<i8> {
            let desc_dim = (5, 5);
            let desc = vec![
                0, 0, 1, 0, 0,
                0, 0, 0, 0, 0,
                1, 0, 2, 0, 1,
                0, 1, 0, 1, 0,
                0, 0, 1, 0, 0,
            ];
            let mut v = vec![0i8; (self.width * self.height) as usize];
            let mut i = 0;
            for y in 0..self.height {
                let dy = f32::floor((y as f32) * (desc_dim.1 as f32) / (self.height as f32)) as usize;
                let dy = dy * desc_dim.1;
                for x in 0..self.width {
                    let dx = f32::floor((x as f32) * (desc_dim.0 as f32) / (self.width as f32)) as usize;
                    v[i] = (desc[dx + dy] * 8) as i8;
                    i += 1;
                }
            }
            return v;
        }

        pub fn tick_height_map(&self, height_map: &mut Vec<i8>) {
            let mut tmp = vec![0_i8; height_map.len()];
            let mut i = 0_usize;
            for y in 0..self.height {
                for x in 0..self.width {
                    let mut cell = height_map[i];
                    if cell >= 4 {
                        cell -= 4;
                    }
                    if x > 0 && height_map[i - 1] >= 4 {
                        cell += 1;
                    }
                    if x < (self.width - 1) && height_map[i + 1] >= 4 {
                        cell += 1;
                    }
                    if y > 0 && height_map[i - self.width as usize] >= 4 {
                        cell += 1;
                    }
                    if y < (self.height - 1) && height_map[i + self.width as usize] >= 4 {
                        cell += 1;
                    }
                    tmp[i] = cell;
                    i += 1;
                }
            }
            height_map.clone_from(&tmp);
        }
    }

    impl Widget for IsoMap {
        type State = State;
        type Style = Style;
        type Event = ();

        fn init_state(&self, id_gen: conrod_core::widget::id::Generator) -> Self::State {
            State {
                height_map: self.shape_height_map(),
                last_update: std::time::Instant::now(),
                cells: self.allocate_cells(),
                ids: Ids::new(id_gen),
            }
        }

        fn style(&self) -> Self::Style {
            self.style.clone()
        }

        fn update(self, args: widget::UpdateArgs<Self>) -> Self::Event {
            #[allow(unused_imports)]
            use conrod_core::{Colorable, Positionable, Sizeable};

            let widget::UpdateArgs { id, state, rect, ui, style: _, .. } = args;

            // ensure we have enough ids for all cubes
            if state.ids.cubes.len() < state.cells.len() {
                let id_gen = &mut ui.widget_id_generator();
                state.update(|state| state.ids.cubes.resize(state.cells.len(), id_gen));
            }

            let now = std::time::Instant::now();
            let elapsed = now.duration_since(state.last_update).as_secs_f64();
            if elapsed > 1.0 {
                state.update(|state| {
                    self.tick_height_map(&mut state.height_map);
                    state.last_update = now;
                });
            }

            let sqrt3_2 = 0.8660254037844; // sqrt(3) / 2
            let m_w = self.width as f64;
            let m_h = self.height as f64;
            let scale_x = f64::min(m_w, m_h) + 0.5 * f64::abs(m_w - m_h);
            let scale_y = scale_x + 1.0;
            let [e_x, e_y] = rect.top_left();
            let (e_w, e_h) = rect.w_h();
            let c_w = e_w / scale_x;
            let c_h = e_h / scale_y;
            let d_x = 0.5 * c_w * sqrt3_2;
            let d_y = 0.25 * c_h;
            let i_x = e_x + 0.5 * e_w;
            let i_y = e_y - 0.5 * c_h - e_h * 0.25;
            for ((i, (x, y)), h) in state.ids.cubes.into_iter().zip(
                (0..self.height).map(|y|
                    (0..self.width).map(move |x|
                        (x, y)
                    )
                ).flatten()
            ).zip(state.height_map.iter()) {
                let x = x as f64;
                let y = y as f64;
                let h = *h as f64;
                iso_cube::IsoCube::new()
                    .uv_rect(x / m_w, y / m_h, 1.0 / m_w, 1.0 / m_h)
                    .parent(id)
                    .length(h - 1.0)
                    .x_y(
                        i_x + d_x * (x - y),
                        i_y - d_y * (x + y)
                    )
                    .w_h(c_w, c_h)
                    .set(*i, ui);
            }
            //iso_cube::IsoCube::new()
            //    .x_y(i_x, i_y)
            //    .w_h(c_w, c_h)
            //    .set(state.ids.dbg_sq0, ui);
            //iso_cube::IsoCube::new()
            //    .length(1.0)
            //    .x_y(i_x + d_x, i_y - d_y)
            //    .w_h(c_w, c_h)
            //    .set(state.ids.dbg_sq2, ui);
            //widget::Rectangle::fill([10.0, 10.0])
            //    .x_y(0.0, e_y)
            //    .w_h(c_w, c_h)
            //    .color(color::rgb(0.0, 1.0, 0.0))
            //    .set(state.ids.dbg_sq0, ui);
            //widget::Rectangle::fill([10.0, 10.0])
            //    .x_y(i_x, 0.0)
            //    .w_h(c_w, c_h)
            //    .color(color::rgb(elapsed as f32, 1.0, 0.0))
            //    .set(state.ids.dbg_sq1, ui);
        }
    }

}
